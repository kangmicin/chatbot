#!/usr/bin/env node

const app = require('express')();
const MessengerPlatform = require('facebook-bot-messenger');
const log = require('simple-node-logger').createSimpleLogger('project.log');
const store = require('./store.js');
const Promise = require('bluebird');
const AsyncBot = require('./botman');
const EventEmitter = require('events');
const myEmitter = new MyEmitter();

class MyEmitter extends EventEmitter {}
let server = require('http').Server(app);
let bot = MessengerPlatform.create({
  pageID: process.env.PAGE_ID,
  appID: process.env.APP_ID,
  appSecret: process.env.APP_SECRET,
  validationToken: process.env.VERIFY_TOKEN,
  pageToken: process.env.PAGE_TOKEN
}, server);

app.get('/callback', function(req, res){
  var message = req.query.queryString;
  var uid = req.query['messenger user id'];
  myEmitter.emit('message', res, message, uid);
})

app.use(bot.webhook('/webhook'));
// Create database instance and start server
var chatfuel = new AsyncBot(myEmitter.on.bind(myEmitter, 'message',
  Promise.coroutine(function* (ctxt, message, uid) {
    bman.getReply(uid, message, function (err, reply) {
      ctxt.json({ messages: [{"text": err || reply }]});
    });
  })))

var bman = new AsyncBot(bot.on.bind(bot, MessengerPlatform.Events.MESSAGE,
  Promise.coroutine(function*(userId, message) {
    // add code below.
    //bot.sendTextMessage(userId, 'Silakan masukan nomer Hp Kalian');
    var profile = yield bot.getProfile(userId);
    var botUser = store.getUserByFid(profile.id);
    // show welcome message
    var message = message.getText()
    log.info('from: '+userId+' message: '+message)
    if (!botUser) {
      botUser = store.addFbUser(profile)
    }
    var reply = bman.getReply(userId, message, function (err, reply) {
      if(err){
        log.error(err)
        bot.sendTextMessage(userId, 'terjadi masalah pada bot, silakan coba lagi '+err)
      }else{
        bot.sendTextMessage(userId, reply)
    }})

  })));
server.listen(8087);
