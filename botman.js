var RiveScript = require('rivescript')
const log = require('simple-node-logger').createSimpleLogger('project.log')
var Promise = require('bluebird')
var firebase = require('firebase')
var XLRequest = require('./XLRequest.js')

var rs = new RiveScript()
firebase.initializeApp({
  databaseURL: 'https://chatbot-9d84a.firebaseio.com'
})

var db = firebase.database();

rs.setSubroutine('loginXl',
Promise.coroutine(function* (rs, args) {
  let msisdn, otp, user, username, session, xl, loginResponse
  
  msisdn = args[0]
  otp = "".toUpperCase.call(args[1])
  username = rs.brain._currentUser
  xl = client[username]
  user = rs._users[username]
  console.log('starting login', msisdn, otp)
  
  loginResponse = yield xl.login(otp, msisdn)
  if(XLRequest.Tags.LOGIN_SUCCESS === loginResponse){
    // console.log('success login')
    session = xl.session
  }
  // console.log('AUTH DONE')
  rs.setUservar(username, 'session', session)

  if(user['paket'] && session) {
    console.log('buy after login')
    purchaseResponse = yield buyPackage(rs, [user['paket']])
    return purchaseResponse
  }
  return loginResponse
}))

rs.setSubroutine('getotp',
Promise.coroutine( function* (rs, args) {
  let xl, otpResponse
  username = rs.brain._currentUser
  xl = client[username]
  // console.log(xl)
  msisdn = args[0]
  otpResponse = yield xl.otp(msisdn)
  console.log("send")
  if(otpResponse === XLRequest.Tags.OTP_SUCCESS){
    return "Kode OTP berhasil dikirimkan melalu sms, silakan masukan kodenya sekarang! (kode hanya aktif selama 5 menit)"
  }
  if(otpResponse === XLRequest.Tags.BANNED_NUMBER){
    return "Kode OTP tidak bisa dikirimkan. \n Pastikan jika kamu tidak berlangganan paket XL GO IZI. Silakan stop paket tersebut agar bisa melanjutkan!"
  }
  return "Kode OTP gagal di kirimkan coba kirimkan lagi atau gunakan nomer lainnya!"
}))

var buyPackage = Promise.coroutine(function* (rs, args) {

  user = rs.brain._currentUser
  xl = client[user]
  paketId = parseInt(rs._users[user]['paket'])
  paket = infoPaket()

  serviceId = paket[paketId] ? paket[paketId].id : paketId
  paketNm = paket[paketId] ? paket[paketId].nama : paketId
  
  purchaseResponse = yield xl.buy(serviceId)

  if(XLRequest.Tags.PACKAGE_PURCASHE === purchaseResponse){
    rs.setUservar(user, 'paket', undefined)
    return 'paket ' + paketNm + ' berhasil diprosess'
  }else{
    return 'paket ' + paketNm + ', gagal diprosess'
  }
})

rs.setSubroutine('beliPaket', buyPackage)

function infoPaket(){
  console.log("lihat paket")
  return db.ref('paket').once('value')
  .then((snap) => {
    console.log(snap.val())
    return snap;
  })
}

rs.setSubroutine('namaPaket', function(rs, args){
  var req = parseInt(args[0])
  var ps = infoPaket()
  var paketNm = ps[req] ? ps[req].nama : req

  return paketNm
})

var infoMessage = function(rs, args) {
  var build = 'berikut daftar paket yang bisa di beli:\n'

  return Promise.resolve(infoPaket().then((snap) => {
    var pakets = snap.val()
    console.log(pakets)
    for (var i = 0, len = pakets.length; i < len; i++) {
      var p = pakets[i]
      build = build + '\t('+ i +') -> '
      + p.nama + ' '
      + 'Rp.' + p.harga+' <'+ p.durasi + ' Hari>'+'\n'
    }
    return build
  }))
}
rs.setSubroutine('infoPaket', infoMessage)


const client = {}
var AsyncBot = function (onReady) {
  let self = this
  
  rs.loadFile('brain.rive', function () {
    rs.sortReplies()
    onReady()
  })

  // This is a function for delivering the message to a user. Its actual
  // implementation could vary for example if you were writing an IRC chatbot
  // this message could deliver a private message to a target username.
  self.sendMessage = function(username, message) {
    // This just logs it to the console like "[Bot] @username: message"
    console.info(["[Kangmicin BOT]", message].join(": "))
  }

  // This is a function for a user requesting a reply. It just proxies through
  // to RiveScript.
  self.getReply = function(username, message, callback) {
    client[username] = client[username] || new XLRequest()
    userData = rs.getUservars(username)
    rs.setUservar(username, 'temp', message)
    // TODO: load user vars from firebase

    return Promise.coroutine(function* () {
      if(!userData){
        userData = yield db.ref('users').child(username).once('value')
        rs.setUservars(username, userData.val())
      }
      db.ref('users').child(username).set(rs.getUservars(username))
      return rs.reply(username, message, bot).then(function(reply){
        callback.call(this, null, reply)
      }).catch(function(error) {
        callback.call(this, error)
      })
    })

  }
}
module.exports = AsyncBot
