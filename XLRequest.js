var axios = require('axios')
var utils = require('./utils')

function XLRequest(msisdn, session, imei) {
  this.msisdn = msisdn;
  this.session = session;
  this.imei = imei || generateImei();
  this.client = axios.create({
    baseURL: 'https://my.xl.co.id',
    headers: {
      'Referer': 'https://my.xl.co.id/pre/index1.html',
      'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'True'
    }
  })
}
var method = XLRequest.prototype;

method.login = function (otpCode, msisdn) {
  var date = (new Date).toISOString().slice(0, 10).replace(/-/g, '')
  , requestId = generateRequestID()
  , body;

  let self = this;
  msisdn = (msisdn || this.msisdn) + "";
  body = {
    Body: {
      Header: {
        ReqID: requestId,
        IMEI: 1837008087
      },
      LoginValidateOTPRq: {
        headerRq: {
          requestDate: date,
          requestId: requestId,
          channel: "MYXLPRELOGIN"
        },
        msisdn: msisdn,
        otp: otpCode
      }
    },
    platform: "04",
    msisdn_Type: "P",
    appVersion: "3.8.1",
    sourceName: "Chrome",
    screenName: "login.enterLoginOTP"
  }
  // console.log(body.Body.LoginValidateOTPRq)
  return this.client
    .post('/pre/LoginValidateOTPRq', body)
    .then(({data}) => {
    var response = data.LoginValidateOTPRs;
    var session = data.sessionId
    // console.log(data)
    if (response.responseCode === '00' && session) {
      self.session = session;
      return XLRequest.Tags.LOGIN_SUCCESS
    }
    self.session = null;
    return XLRequest.Tags.LOGIN_FAILED
  })
}
method.otp = function (msisdn) {
  let body, self = this, requestId, imei;

  requestId = generateRequestID()
  imei = this.imei
  msisdn = this.msisdn
  body = {
    Body: {
      Header: {
        ReqID: requestId,
        IMEI: imei
      },
      LoginSendOTPRq: {
        msisdn: msisdn
      }
    },
    onNet: 'False',
    platform: '04',
    appVersion: '3.8.1',
    sourceName: 'Chrome',
    screenName: 'login.enterLoginNumber'
  }
  // console.log(body.Body)
  return this.client.post('/pre/LoginSendOTPRq', body)
    .then(({data}) => {
      // console.log(data)
      if(data.responseCode === '01' || data.responseCode === '33' || data.responseCode === '08'){
        return XLRequest.Tags.BANNED_NUMBER
      }
      var status = data.LoginSendOTPRs.headerRs
      if (status.responseCode === '08') {
        return XLRequest.Tags.MANY_REQUEST
      } else if(status.responseCode === '00'){
        self.msisdn = msisdn
        return XLRequest.Tags.OTP_SUCCESS
      }
      else{
        return XLRequest.Tags.SERVER_ERROR
      }
    })
}
method.buy = function (serviceId, sessionId, msisdn) {
  var requestId = generateRequestID()
  , body
  , mMsisdn = (this.msisdn || msisdn) + ""
  , mSessionId = this.session || sessionId
  , mServiceId = serviceId + ""

  body = {
    Body: {
      HeaderRequest: {
        applicationID: "3",
        applicationSubID: "1",
        touchpoint: "MYXL",
        requestID: requestId,
        msisdn: mMsisdn,
        serviceID: mServiceId
      },
      opPurchase: {
        msisdn: mMsisdn,
        serviceid: serviceId
      },
      XBOXRequest: {
        requestName: "GetSubscriberMenuId",
        Subscriber_Number: "2099159844"
      },
      Header: {
        IMEI: 1837008087,
        ReqID: requestId
      }
    },
    sessionId: mSessionId,
    serviceId: mServiceId,
    packageRegUnreg: "Reg",
    platform: "04",
    appVersion: "3.8.1",
    sourceName: "Chrome",
    msisdn_Type: "P",
    screenName: "home.storeFrontReviewConfirm"
  }
  return this
    .client.post('/pre/opPurchase', body)
    .then(({data}) => {
      // console.log(data)
      if (isPurchased(data)) {
        return XLRequest.Tags.PACKAGE_PURCASHE
      }
      return XLRequest.Tags.PACKAGE_REJECT
    })
}
XLRequest.Tags = {
  LOGIN_SUCCESS : 'berhasil login',
  LOGIN_FAILED : 'gagal login',
  SERVER_ERROR : 'sedang ada masalah di sistem!',
  MANY_REQUEST : 'Tidak bisa mengirim sandi, tunggu beberapa menit!',
  OTP_SUCCESS : 'sandi berhasil dikirimkan!',
  PACKAGE_PURCASHE : 'berhasil memproses paket!',
  PACKAGE_REJECT : 'gagal membeli paket!',
  BANNED_NUMBER : 'banned number'
}

function generateImei() {
  var MIN_IMEI = 821000000,
    MAX_IMEI = 900000000;
  return Math.floor(Math.random() * (MAX_IMEI - MIN_IMEI)) + MIN_IMEI;
}
function generateRequestID() {
  var a = new Date
    , b = "";
  return void 0 != a && (b = a.getFullYear() + "" + checkDateString(a.getMonth() + 1) + checkDateString(a.getDate()) + checkDateString(a.getHours()) + checkDateString(a.getMinutes()) + checkDateString(a.getSeconds())),
  b
}
function checkDateString(a) {
  return Number(a) <= 9 && (a = "0" + a),
  a
}
function isPurchased(b) {
  var c = {};
  // console.log(b["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0]["ns0:opPurchaseRs"][0]["ns0:Status"])
  return b && void 0 != b["SOAP-ENV:Envelope"] && (c = b["SOAP-ENV:Envelope"]),
  void 0 == c || void 0 == c["SOAP-ENV:Body"] || void 0 == c["SOAP-ENV:Body"][0] || void 0 == c["SOAP-ENV:Body"][0]["ns0:opPurchaseRs"] || void 0 == c["SOAP-ENV:Body"][0]["ns0:opPurchaseRs"][0] || "IN PROGRESS" != c["SOAP-ENV:Body"][0]["ns0:opPurchaseRs"][0]["ns0:Status"] && "SUCCESS" != c["SOAP-ENV:Body"][0]["ns0:opPurchaseRs"][0]["ns0:Status"] ? false : true;
}
module.exports = XLRequest;