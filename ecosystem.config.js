module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'API',
      script    : 'index.js',
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production : {
        NODE_ENV: 'production',
        VERIFY_TOKEN: 'novi421,',
        PAGE_TOKEN: 'EAAWsX7YWmPsBADtPOF4ZC0TuAeJEXhNaL1GXmuGR2pVnxQ0h9cQThLxVGZApzOLRJPZA4HP60hOcYyRpNBWZCfDrPTkqNyTt4CYdIhOBjAzJ8XXls1zTLEbE1qUMfgzGiPZB9VLkPdWrfONGjJF2zEOK1MPfgZAk14kKX6utWO9QF68ZAy6w6mZC',
        APP_SECRET: '4fa1245862135b6c99e04c0c132410d6',
        PAGE_ID: '1665009820279996',
        APP_ID: '1596901960358139'
      }
    },
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    },
    dev : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/development',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env dev',
      env  : {
        NODE_ENV: 'dev'
      }
    }
  }
};
