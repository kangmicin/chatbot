const FileSync = require('lowdb/adapters/FileSync')

const Promise = require('bluebird');
const _ = require('lodash')
const format = require('dateformat');
const sformat = require('string-format')
const db = require("./db.json");


var addFbUser = function ({id, first_name, last_name, profile_pic, locale, timezone, gender}) {
    db.get('users').push({
      'fid': id,
      'first_name': first_name,
      'last_name': last_name,
      'profile_pic': profile_pic,
      'locale': locale,
      'timezone': timezone || 8,
      'gender': gender
      }).write()
    return getUserByFid(id);
  }
var getUserByFid = function (id) {
  let fid, phone, password, session, action, wait;
  let q = db.get('users').find({fid: id})
  let v = q.value();
  if(!v) return undefined;
  let user = {};
  fid = v.fid; phone = v.sisdn, sendMessage=v.wait;
      password = v.password; session = v.session; action = v.action;
   Object.defineProperty(user, 'fid', {value: fid})
   return user;
  }

module.exports = {
  getUserByFid: getUserByFid,
  addFbUser: addFbUser
}
