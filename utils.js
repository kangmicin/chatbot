var CryptoJS = require('./aes.js');


var enc = function enc(str) {
  var d = "JsY1dPbldCJndHdGhmFFI.,pubAdJYkQGAGlhQkxdYAM.,IsiPulsaxlviamyxl10928375a".split(","),
    e = d[0],
    f = d[1],
    a = str,
    e = CryptoJS.enc.Base64.parse(e),
    f = CryptoJS.enc.Base64.parse(f);
  var g = CryptoJS.AES.encrypt(a, e, {
    iv: f
  });
  // console.log(d[0])
  // console.log(d[1])
  // console.log(e.toString(), '..', e.toString().length,'--', f.toString())
  return g.toString();
}
function randomString(a, b) {
  var c = "";
  b.indexOf("a") > -1 && (c += "abcdefghijklmnopqrstuvwxyz"),
  b.indexOf("A") > -1 && (c += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
  b.indexOf("#") > -1 && (c += "0123456789"),
  b.indexOf("!") > -1 && (c += "~`!@#$%^&*()_+-={}[]:\";'<>?,./|\\");
  for (var d = "", e = a; e > 0; --e)
    d += c[Math.round(Math.random() * (c.length - 1))];
  return d
}
function randomNumber() {
  return Math.floor(1e8 + 9e8 * Math.random())
}
// console.log(enc('mX37Cl'))
module.exports = {
  enc, randomString, randomNumber
}
